# zero2prod-axum


## What is this project?

This is a personal practice project trying to replicate the [Zero To Production In Rust](https://www.zero2prod.com/index.html)
but with a different web framework [Axum](https://crates.io/crates/axum) and other latest crates.
Other differences including different Pipeline service (GitLab CI instead of GitHub Actions),
potentially different Deploy service (originally [Digital Ocean](https://www.digitalocean.com), not decided yet at this moment), etc.

## License

MIT License is applied.
